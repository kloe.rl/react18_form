Modalités pédagogiques

Créer un nouveau projet vite / react
Ajouter un composant 'Form.jsx' dans 'App.jsx'
Créer un formulaire d'inscription contrôlé, sans react-hook-form pour recueillir le nom, le prénom, l'âge, l'adresse email et le mot de passe d'un nouvel utilisateur.
Créer une fonction de contrôle sur l'âge de la personne. L'âge devra être supérieur à 18 ans. Si l'âge est inférieur, un message doit apparaître dans le formulaire.
Vérifier qu'aucune erreur n'apparaît dans le terminal
Un dépôt GitLab contient le code du projet