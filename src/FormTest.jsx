import React from 'react'
import { useState } from 'react'

import './form.css'

function FormTest() {

    const [input, setInput] = useState({ firstName: "", lastName: "", age: "", email: "", password: "" })
    // const [input, setInput] = useState("")
    const [errorMessage, setErrorMessage] = useState(null)
    const [errorAgeMessage, setErrorAgeMessage] = useState(null)

    const handleChange = (e) => {
        const { name, value } = e.target
        setInput((prevInput) => ({ ...prevInput, [name]: value }))
        if (input.age < 18) {
            setErrorAgeMessage("Age must be 18 or more")
        } else {
            setErrorAgeMessage(null)
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault()
        if (input.firstName.length > 0 &&
            input.lastName.length > 0 &&
            input.age.length > 0 &&
            input.email.length > 0 &&
            input.password.length > 0) {
            setErrorMessage(null)
        } else {
            setErrorMessage('Input must not be empty')
        }
        console.log(`FirstName: ${input.firstName}, LastName: ${input.lastName}, age: ${input.age}, email: ${input.email}, password: ${input.password}`)
    }

    return (
        <>
            {/* Form which retrieves lastname, firstname, age, email, password */}
            <form onSubmit={handleSubmit}>
                <div className="div">
                    <label>First Name</label>
                    <input name="firstName" type="text" value={input.firstName}
                        onChange={handleChange}
                    />
                </div>
                <div className="div">
                    <label>Last Name</label>
                    <input name="lastName" type="text" value={input.lastName}
                        onChange={handleChange}
                    />
                </div>
                <div className="div">
                    <label>Age</label>
                    <input name="age" type="number" value={input.age}
                        onChange={handleChange}
                    />
                </div>
                <div className="div">
                    <label>Email</label>
                    <input name="email" type="text" value={input.email}
                        onChange={handleChange}
                    />
                </div>
                <div className="div">
                    <label>Password</label>
                    <input name="password" type="text" value={input.password}
                        onChange={handleChange}
                    />
                </div>
                <button type="submit">Submit</button>
                {errorAgeMessage && <div >{errorAgeMessage}</div>}
                {errorMessage && <div style={{ color: 'red' }}>{errorMessage}</div>}
            </form >
        </>
    )
}

export default FormTest